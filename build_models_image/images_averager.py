#!/usr/bin/env python
import cv2
import os

##  Averager:
##    Params: cursor    ->  the image the user is on
##            path      ->  path to the folder containing the images
##
##    Return: The image containing the two previous and the current

def averager(cursor, path):

  path_end = ".png"

  tmp = cv2.imread(path + str(cursor) + path_end)
  final = cv2.imread(path + str(cursor) + path_end)

  if cursor > 1:
    prev = cv2.imread(path + str(cursor - 1) + path_end)
    if cursor > 2:
      pprev = cv2.imread(path + str(cursor - 2) + path_end)
      tmp = cv2.addWeighted(tmp, 0.3, pprev, 0.7, 0.0, tmp)
    tmp = cv2.addWeighted(tmp, 0.3, prev, 0.7, 0.0, tmp)

  final = cv2.addWeighted(final, 0.5, tmp, 0.5, 0.0, final)

  return final

path = "../images/pap_7/"
img = averager(5, path)
cv2.imshow('moyenne', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
