import RPi.GPIO as GPIO
import cv2

GPIO.setmode(GPIO.BCM)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def record_pressed():
    print("record pressed")

def prev_pressed():
    print("prev pressed")

def next_pressed():
    print("next pressed")

def delete_pressed():
    print("delete pressed")

def play_pressed():
    print("play pressed")

def stop_pressed():
    print("stop pressed")

def save_pressed():
    print("save pressed")

def waitpressedbutton():
    while True:
        record_button = GPIO.input(4)
        prev_button = GPIO.input(17)
        next_button = GPIO.input(18)
        delete_button = GPIO.input(27)
        play_button = GPIO.input(22)
        stop_button = GPIO.input(23)
        save_button = GPIO.input(24)
        
        if (record_button == False):            
            print "record"
            return "record"
        
        if (prev_button == False):
            print "prev"
            return "prev"

        if (next_button == False):
            print "next"
            return "next"

        if (delete_button == False):
            print "delete"
            return "delete"

        if (play_button == False):
            print "play"
            return "play"

        if (stop_button == False):
            print "stop"
            return "stop"

        if (save_button == False):
            print "save"
            return "save"

