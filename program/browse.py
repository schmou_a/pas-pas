#!/usr/bin/env python
import cv2
import os.path
import numpy as np
import selection
import playing
import button
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def browse_mode():

    cartoon_selector = 0
    cartoon = []
    cartoon_cursor = 1

    # Les chemins seront a remplacer par le chemin des
    # cartoons captures via le mode assiste ou libre.
    cartoons_number = selection.count_folders("/media/PAP/models/")
    c_path = "/media/PAP/models/pap_"

    if not os.path.exists("/media/PAP/models/"):
        cv2.imshow("window", cv2.imread("./load_failed.png"))
        cv2.waitKey(0)
        return

    while (len(cartoon) == 0):
        selection.build_selection(cartoon_selector, c_path, cartoons_number)
        cv2.waitKey(100)
        button_value = button.waitpressedbutton()
        if (button_value == "next"): # next
            cartoon_selector += 1
        elif (button_value == "prev"): # prev
            cartoon_selector -= 1
        elif (button_value == "play"): # play
            cartoon = playing.load_selected(c_path + str(cartoon_selector % cartoons_number + 1) + "/")
            playing.play(cartoon)


browse_mode()
cv2.destroyAllWindows()
