#!/usr/bin/env python
import cv2
import selection
import playing
import camera
import image
import button
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_UP) # switch cercle
GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def free_mode():

    takes_list = []
    background = cv2.imread("./background.png", 1)

    while ("true"):

        mode = GPIO.input(13) # lecture du switch cercle

        # determine l'image a afficher avant d'attendre le bouton
        # si la liste de prises est vide ou qu'on est en mode normal
        # on affiche du noir.
        # Sinon, on affiche les images precedentes, jusqu'a 3

        if (mode == False or len(takes_list) < 1):
            cv2.imshow("window", background)
        else:
            cv2.imshow("window", image.averager(takes_list, takes_list[len(takes_list) - 1])) # affiche la moyenne des 3 images en partant de la derniere

        button_value = button.waitpressedbutton()
        cv2.waitKey(100)
        # Lire le selecteur de mode (libre, assiste ou browse)
        # si != free, return

        if (button_value == "record"):                        # prendre l'image
            takes_list.append(camera.get_image())
            cv2.imshow("window", takes_list[len(takes_list) - 1])
            cv2.waitKey(200)
            takes_list.append(camera.get_image())
        if (button_value == "delete" and len(takes_list) > 0): # supprimer la derniere image
            takes_list.pop[len(takes_list) - 1]
        if (button_value == "save"):                        # sauvegarder le cartoon et quitter
            # Detecter la presence de la cle.
            # Si elle est la, sauvegarder et return.
            # Sinon, afficher save_failed.png puis continuer.
            playing.save(takes_list)
        if (button_value == "play"):                        # jouer le cartoon
            playing.play(takes_list)


free_mode()
