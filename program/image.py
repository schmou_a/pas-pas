#!/usr/bin/env python
import cv2
import os

##  Averager:
##    Params: cursor    ->  the image the user is on
##            path      ->  path to the folder containing the images
##
##    Return: The image containing the two previous and the current

def averager(cursor, cartoon):

  tmp = cartoon[cursor]
  final = cartoon[cursor]
  if (cursor > 1):
    prev = cartoon[cursor - 1]
    if (cursor > 2):
      pprev = cartoon[cursor - 2]
      tmp = cv2.addWeighted(tmp, 0.3, pprev, 0.7, 0.0, tmp)
    tmp = cv2.addWeighted(tmp, 0.3, prev, 0.7, 0.0, tmp)

  final = cv2.addWeighted(final, 0.5, tmp, 0.5, 0.0, final)

  return final
