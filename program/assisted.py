#!/usr/bin/env python
import cv2
import selection
import playing
import camera
import image
import button
import os.path
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(6, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # switch triangle
GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def assisted_mode():

    model_list = selection.select_cartoon("/media/PAP/models/")
    takes_list = []
    model_cursor = 0
    takes_cursor = 0

    while ("true"):

        if GPIO.input(6) == False or len(takes_list) < 1:
            cv2.imshow("window", model_list[model_cursor])
            cv2.waitKey(100)
        else:
            cv2.imshow("window", takes_list[takes_cursor])
            cv2.waitKey(100)
        button_value = button.waitpressedbutton()

        # Lire le selecteur de mode (libre, assiste ou browse)
        # si != free, return

        if (button_value == "record"):                        # prendre l'image
            cv2.imshow("window", background)
            cv2.waitKey(100)
            takes_list.append(camera.get_image())
            if (display_mode == True):
                model_cursor += 1
            else:
                takes_cursor += 1
            cv2.imshow("window", take_list[takes_cursor])
            cv2.waitKey(500)

        if (button_value == "delete" and len(takes_list) > 0): # supprimer la derniere image
            takes_list.pop[len(takes_list) - 1]
        if (button_value == "save"):
            if not os.path.exists("/media/PAP/cartoons"):
                cv2.imshow("window", cv2.imread("./save_failed"))
                cv2.waitKey(100)
            else:
                save(takes_list)
                return
        if (button_value == "play"):                        # jouer le cartoon
            playing.play(takes_list)
        if (button_value == "next"):                        # next
            if (display_mode == True):
                model_cursor += 1
            else:
                takes_cursor += 1
        if (button_value == "prev"):                        # prev
            if (display_mode == True):
                model_cursor -= 1
            else:
                takes_cursor -= 1

assisted_mode()
cv2.destroyAllWindows()
