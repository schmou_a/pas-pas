#!/usr/bin/env python
import cv2
import selection
import os
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def load_selected(path):

    img_list = []
    img_number = selection.count_files(path) + 1
    path_end = ".png"

    # On ajoute les images une par une a la liste d'images
    for i in range (1, img_number):
        img_list.append(cv2.imread((path + str(i) + path_end), 1))
    for i in range (0, img_number - 1):
        img_list[i] = cv2.resize(img_list[i], None, fx=2, fy=2, interpolation=cv2.INTER_LINEAR)
    return img_list

def play(cartoon):

    c_len = len(cartoon)
    i = 0
    
    while ("true"):
        while (i < c_len):
            cv2.imshow("window", cartoon[i])
            i += 1
            if GPIO.input(23) == False:
                return
            cv2.waitKey(200)

        # loop mode
        if (GPIO.input(19) == False):
            i = 0

        # back and forth
        else:
            i -= 2
            while (i > 0):
                cv2.imshow("window", cartoon[i])
                if GPIO.input(23) == False:
                    return
                button_value = cv2.waitKey(200)
                i -= 1

def save(cartoon):
    
    c_number = selection.count_folders("/media/PAP/cartoons/")
    os.makedirs("/media/PAP/cartoons/pap_" + str(c_number + 1))
    for i in range (0, len(cartoon)):
        cv2.imwrite("/media/PAP/cartoons/pap_" + str(c_number + 1) + "/" + str(i + 1) + ".png", cartoon[i]);
